# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.RoomTest do
  use ExUnit.Case
  doctest Polyjuice.Client.Room

  test "send message" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutRoomsSend{
               room: "!bar",
               txn_id: "txn_id",
               event_type: "m.room.message",
               message: %{
                 "msgtype" => "m.text",
                 "body" => "foo"
               }
             },
             {:ok, "$foo1"}
           }
         } do
      {:ok, event_id} = Polyjuice.Client.Room.send_message(client, "!bar", "foo")
      assert event_id == "$foo1"
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutRoomsSend{
               room: "!bar",
               txn_id: "txn_id",
               event_type: "m.room.message",
               message: %{
                 "msgtype" => "m.text",
                 "formatted_body" => "<i>foo</i>",
                 "format" => "org.matrix.custom.html",
                 "body" => "foo"
               }
             },
             {:ok, "$foo2"}
           }
         } do
      {:ok, event_id} = Polyjuice.Client.Room.send_message(client, "!bar", {"foo", "<i>foo</i>"})
      assert event_id == "$foo2"
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutRoomsSend{
               room: "!bar",
               txn_id: "txn_id",
               event_type: "m.room.message",
               message: %{
                 "msgtype" => "m.notice",
                 "body" => "foo"
               }
             },
             {:ok, "$foo3"}
           }
         } do
      {:ok, event_id} =
        Polyjuice.Client.Room.send_message(client, "!bar", %{
          "msgtype" => "m.notice",
          "body" => "foo"
        })

      assert event_id == "$foo3"

      # trying to send a non-msgdata should error
      assert_raise ArgumentError, fn ->
        Polyjuice.Client.Room.send_message(client, "!bar", 1)
      end

      assert_raise ArgumentError, fn ->
        Polyjuice.Client.Room.send_message(client, "!bar", client)
      end

      assert_raise FunctionClauseError, fn ->
        Polyjuice.Client.Room.send_message(client, "!bar", {"a"})
      end
    end
  end

  test "send event" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutRoomsSend{
               room: "!bar",
               txn_id: "txn_id",
               event_type: "m.room.message",
               message: %{
                 "msgtype" => "m.text",
                 "body" => "foo"
               }
             },
             {:ok, "$foo1"}
           }
         } do
      {:ok, event_id} =
        Polyjuice.Client.Room.send_event(
          client,
          "!bar",
          "m.room.message",
          %{"msgtype" => "m.text", "body" => "foo"}
        )

      assert event_id == "$foo1"
    end
  end

  test "send state event" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutRoomsState{
               room: "!bar",
               event_type: "m.room.name",
               state_key: "",
               content: %{
                 "name" => "foo"
               }
             },
             {:ok, "$foo1"}
           }
         } do
      {:ok, event_id} =
        Polyjuice.Client.Room.send_state_event(
          client,
          "!bar",
          "m.room.name",
          "",
          %{"name" => "foo"}
        )

      assert event_id == "$foo1"

      {:ok, ^event_id} =
        Polyjuice.Client.Room.send_state_event(
          client,
          "!bar",
          "m.room.name",
          %{"name" => "foo"}
        )
    end
  end

  test "set typing" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutRoomsTyping{
               room: "!bar",
               user_id: "@alice:example.org",
               typing: true
             },
             :ok
           }
         } do
      :ok =
        Polyjuice.Client.Room.set_typing(
          client,
          "!bar",
          true
        )
    end
  end

  test "get all state events" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetRoomsState{
               room: "!bar",
               event_type: nil,
               state_key: ""
             },
             {:ok, [%{"type" => "m.room.name", "content" => %{"name" => "foo1"}}]}
           }
         } do
      {:ok, events_list} =
        Polyjuice.Client.Room.get_state(
          client,
          "!bar",
          nil,
          ""
        )

      assert events_list == [%{"type" => "m.room.name", "content" => %{"name" => "foo1"}}]
    end
  end

  test "get one state event" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetRoomsState{
               room: "!bar",
               event_type: "m.room.name",
               state_key: ""
             },
             {:ok, %{"name" => "foo1"}}
           }
         } do
      {:ok, event_content} =
        Polyjuice.Client.Room.get_state(
          client,
          "!bar",
          "m.room.name",
          ""
        )

      assert event_content == %{"name" => "foo1"}
    end
  end

  test "update read receipt" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostRoomsReceipt{
               room: "!room",
               event_id: "$event",
               receipt_type: "m.read"
             },
             :ok
           }
         } do
      :ok = Polyjuice.Client.Room.update_read_receipt(client, "!room", "$event", "m.read")
      :ok = Polyjuice.Client.Room.update_read_receipt(client, "!room", "$event")
    end
  end

  test "update read markers" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostRoomsReadMarkers{
               room: "!room",
               fully_read: "dede:local.fr",
               read: "toto:local.fr"
             },
             :ok
           }
         } do
      :ok =
        Polyjuice.Client.Room.update_read_markers(
          client,
          "!room",
          "dede:local.fr",
          "toto:local.fr"
        )
    end
  end

  test "get_members" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetRoomsMembers{
               room: "!room",
               at: nil,
               membership: nil,
               not_membership: nil
             },
             members =
               {:ok,
                [
                  %{
                    "age" => 46_200_066,
                    "content" => %{
                      "displayname" => "alice",
                      "membership" => "join"
                    },
                    "event_id" => "$TN1CfJlXVOJb2IVClcdu5b1ucXHZOxXUh9BkLR0QG4Q",
                    "origin_server_ts" => 1_630_350_697_874,
                    "replaces_state" => "$RANTgxd1-4Qn0UlgCVqZ6Yeq_x5ntei8f6laCfpD4pc",
                    "room_id" => "!LcFNwPwfBOiiZDxLtX:example.org",
                    "sender" => "@alice:example.org",
                    "state_key" => "@alice:example.org",
                    "type" => "m.room.member",
                    "unsigned" => %{
                      "age" => 46_200_066,
                      "replaces_state" => "$RANTgxd1-4Qn0UlgCVqZ6Yeq_x5ntei8f6laCfpD4pc"
                    },
                    "user_id" => "@alice:example.org"
                  },
                  %{
                    "age" => 46_203_939,
                    "content" => %{
                      "displayname" => "bob",
                      "membership" => "join"
                    },
                    "event_id" => "$MYbq9odFafo7rmlULG5LTUgW2cK5N86e5qXyH5Z8wvY",
                    "origin_server_ts" => 1_630_350_694_001,
                    "replaces_state" => "$sII85oWicRvPL25wykwJggXkzRR6J8XNPcrkWlOYzbQ",
                    "room_id" => "!LcFNwPwfBOiiZDxLtX:example.org",
                    "sender" => "@bob:example.org",
                    "state_key" => "@bob:example.org",
                    "type" => "m.room.member",
                    "unsigned" => %{
                      "age" => 46_203_939,
                      "replaces_state" => "$sII85oWicRvPL25wykwJggXkzRR6J8XNPcrkWlOYzbQ"
                    },
                    "user_id" => "@bob:example.org"
                  }
                ]}
           }
         } do
      assert members ==
               Polyjuice.Client.Room.get_members(
                 client,
                 "!room"
               )
    end
  end

  test "get_joined_members" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetRoomsJoinedMembers{
               room: "!room"
             },
             joined =
               {:ok,
                %{
                  "@alice:example.org" => %{
                    "avatar_url" => "mxc://example.org/aabbccddeeffgg",
                    "display_name" => "alice"
                  },
                  "@bob:example.org" => %{
                    "avatar_url" => "mxc://example.org/ggffeeddccbbaa",
                    "display_name" => "bob"
                  }
                }}
           }
         } do
      assert joined ==
               Polyjuice.Client.Room.get_joined_members(
                 client,
                 "!room"
               )
    end
  end

  test "join room" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostJoin{
               room: "!room",
               servers: [],
               third_party_signed: nil
             },
             {:ok, "!room"}
           }
         } do
      {:ok, "!room"} = Polyjuice.Client.Room.join(client, "!room")
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostJoin{
               room: "!room",
               servers: ["example.org"],
               third_party_signed: %{}
             },
             {:ok, "!room"}
           }
         } do
      {:ok, "!room"} = Polyjuice.Client.Room.join(client, "!room", ["example.org"], %{})
    end
  end

  test "leave room" do
    client = %DummyClient{
      response: {
        %Polyjuice.Client.Endpoint.PostRoomsLeave{room: "!room"},
        :ok
      }
    }

    :ok = Polyjuice.Client.Room.leave(client, "!room")
  end

  test "forget room" do
    client = %DummyClient{
      response: {
        %Polyjuice.Client.Endpoint.PostRoomsForget{room: "!room"},
        :ok
      }
    }

    :ok = Polyjuice.Client.Room.forget(client, "!room")
  end

  test "get messages" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetRoomsMessages{
               room: "!room",
               from: "token",
               dir: :backward
             },
             {:ok, %{}}
           }
         } do
      {:ok, %{}} = Polyjuice.Client.Room.get_messages(client, "!room", "token", :backward)
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetRoomsMessages{
               room: "!room",
               from: "token",
               dir: :backward,
               to: "end_token",
               limit: 20,
               filter: %{}
             },
             {:ok, %{}}
           }
         } do
      {:ok, %{}} =
        Polyjuice.Client.Room.get_messages(
          client,
          "!room",
          "token",
          :backward,
          to: "end_token",
          limit: 20,
          filter: %{}
        )
    end
  end

  test "stream messages" do
    with client =
           DummyClient.MultiReq.create([
             {
               %Polyjuice.Client.Endpoint.GetRoomsMessages{
                 room: "!room",
                 from: "token1",
                 dir: :backward
               },
               {:ok,
                %{
                  "start" => "token1",
                  "end" => "token2",
                  "chunk" => [
                    "event1",
                    "event2"
                  ]
                }}
             },
             {
               %Polyjuice.Client.Endpoint.GetRoomsMessages{
                 room: "!room",
                 from: "token2",
                 dir: :backward
               },
               {:ok,
                %{
                  "start" => "token2",
                  "end" => "token2",
                  "chunk" => []
                }}
             }
           ]) do
      events =
        Polyjuice.Client.Room.stream_messages(client, "!room", "token1", :backward)
        |> Enum.to_list()

      assert events == [
               %{
                 "start" => "token1",
                 "end" => "token2",
                 "chunk" => [
                   "event1",
                   "event2"
                 ]
               }
             ]

      Polyjuice.Client.API.stop(client)
    end
  end

  test "create_room" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostCreateRoom{},
             {:ok, "!room"}
           }
         } do
      {:ok, "!room"} = Polyjuice.Client.Room.create_room(client)
    end
  end

  test "create alias" do
    room_id = "!id_roomtest:homeserver"
    room_alias = "#new_room_name:homeserver"

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutRoomAlias{
               room_id: room_id,
               room_alias: room_alias
             },
             {:ok, room_alias}
           }
         } do
      {:ok, ^room_alias} = Polyjuice.Client.Room.create_alias(client, room_id, room_alias)
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutRoomAlias{
               room_id: room_id,
               room_alias: room_alias
             },
             {:ok, room_alias}
           }
         } do
      room_alias = "#new_room_name:homeserver"
      {:ok, ^room_alias} = Polyjuice.Client.Room.create_alias(client, room_id, room_alias)
    end
  end

  test "get alias" do
    room_id = "id_roomtest:homeserver"
    room_alias = "#new_room_name:homeserver"

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetRoomAlias{
               room_alias: room_alias
             },
             {:ok, {room_id, %{}}}
           }
         } do
      {:ok, {^room_id, %{}}} = Polyjuice.Client.Room.get_alias(client, room_alias)
    end
  end
end
