# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.MediaTest do
  use ExUnit.Case
  doctest Polyjuice.Client.Room

  defmodule Httpd do
    def _matrix(session_id, env, input) do
      # FIXME: check authorization header
      # FIXME: check method
      [path | _] =
        Keyword.get(env, :path_info)
        |> to_string()
        |> String.split("?", parts: 2)

      case path do
        "media/r0/upload" ->
          handle_upload(session_id, env, input)

        "media/r0/download/example.org/foo" ->
          handle_download(session_id, env, input)

        _ ->
          :mod_esi.deliver(
            session_id,
            'Status: 404 Not Found\r\nContent-Type: application/json\r\n\r\n{"errcode":"M_NOT_FOUND","error":"Not found"}'
          )
      end
    end

    defp handle_upload(session_id, _env, {_, input}) do
      {:ok, file} = File.read("mix.exs")

      if to_string(input) == file do
        :mod_esi.deliver(
          session_id,
          'Content-Type: application/json\r\n\r\n{"content_uri":"mxc://example.org/abcdefg"}'
        )
      else
        :mod_esi.deliver(
          session_id,
          'Status: 400 Bad Request\r\nContent-Type: application/json\r\n\r\n{"errcode":"M_UNKNOWN","error":"Wrong contents"}'
        )
      end
    end

    defp handle_download(session_id, _env, _input) do
      :mod_esi.deliver(
        session_id,
        'Content-Type: text/plain\r\nContent-Disposition: attachment; filename="foo.txt"\r\n\r\nfoo'
      )
    end
  end

  test "upload" do
    {:ok, tmpdir} = TestUtil.mktmpdir("sync-")

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.Client.MediaTest.Httpd]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      {:ok, client_pid} =
        Polyjuice.Client.start_link(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.MediaTest.Httpd",
          access_token: "an_access_token",
          user_id: "@alice:example.org",
          sync: false,
          test: true
        )

      client = Polyjuice.Client.get_client(client_pid)

      {:ok, url} = Polyjuice.Client.Media.upload(client, {:file, "mix.exs"})

      assert url == "mxc://example.org/abcdefg"

      {:ok, filename, content_type, body} =
        Polyjuice.Client.Media.download(client, "mxc://example.org/foo")

      assert filename == "foo.txt"
      assert content_type == "text/plain"
      assert Enum.join(body) == "foo"

      Polyjuice.Client.API.stop(client)

      :inets.stop(:httpd, httpd_pid)
    after
      File.rm_rf(tmpdir)
    end
  end
end
