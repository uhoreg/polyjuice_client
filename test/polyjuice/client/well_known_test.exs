# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.WellKnownTest do
  use ExUnit.Case
  alias Polyjuice.Client.WellKnown

  defmodule Httpd do
    def _matrix(session_id, env, _input) do
      [path | _] =
        Keyword.get(env, :path_info)
        |> to_string()
        |> String.split("?", parts: 2)

      case path do
        "client/versions" ->
          :mod_esi.deliver(
            session_id,
            'Content-Type: application/json\r\n\r\n{"versions": ["foobar"]}'
          )

        "identity/api/v1" ->
          :mod_esi.deliver(
            session_id,
            'Content-Type: application/json\r\n\r\n{}'
          )

        _ ->
          :mod_esi.deliver(
            session_id,
            'Status: 404 Not Found\r\nContent-Type: application/json\r\n\r\n{"errcode":"M_NOT_FOUND","error":"Not found"}'
          )
      end
    end

    # various bad responses
    def notfound(session_id, _, _) do
      :mod_esi.deliver(
        session_id,
        'Status: 404 Not Found\r\nContent-Type: application/json\r\n\r\n{"errcode":"M_NOT_FOUND","error":"Not found"}'
      )
    end

    def missingversions(session_id, _, _) do
      :mod_esi.deliver(
        session_id,
        'Content-Type: application/json\r\n\r\n{}'
      )
    end

    def notjson(session_id, _, _) do
      :mod_esi.deliver(
        session_id,
        'Content-Type: text/plain\r\n\r\nfoo'
      )
    end
  end

  test "get homeserver" do
    {:ok, tmpdir} = TestUtil.mktmpdir("client-call-")

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.Client.WellKnownTest.Httpd]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      assert WellKnown.get_homeserver(%{}) == {:error, :fail_prompt}
      assert WellKnown.get_homeserver(%{"m.homeserver" => %{}}) == {:error, :fail_prompt}

      assert WellKnown.get_homeserver(%{
               "m.homeserver" => %{
                 "base_url" =>
                   "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd"
               }
             }) == {:ok, "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/"}

      assert WellKnown.get_homeserver(%{
               "m.homeserver" => %{
                 "base_url" =>
                   "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/notfound/"
               }
             }) == {:error, :fail_error}

      assert WellKnown.get_homeserver(%{
               "m.homeserver" => %{
                 "base_url" =>
                   "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/missingversions"
               }
             }) == {:error, :fail_error}

      assert WellKnown.get_homeserver(%{
               "m.homeserver" => %{
                 "base_url" =>
                   "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/notjson"
               }
             }) == {:error, :fail_error}

      assert WellKnown.get_identity_server(%{}) == {:ok, nil}

      assert WellKnown.get_identity_server(%{"m.identity_server" => "foo"}) ==
               {:error, :fail_error}

      assert WellKnown.get_identity_server(%{
               "m.identity_server" => %{
                 "base_url" =>
                   "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd"
               }
             }) == {:ok, "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/"}

      assert WellKnown.get_identity_server(%{
               "m.identity_server" => %{
                 "base_url" =>
                   "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/notfound/"
               }
             }) == {:error, :fail_error}

      # lacking a "versions" property in identity server is normal
      assert WellKnown.get_identity_server(%{
               "m.identity_server" => %{
                 "base_url" =>
                   "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/missingversions"
               }
             }) ==
               {:ok,
                "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/missingversions/"}

      assert WellKnown.get_identity_server(%{
               "m.identity_server" => %{
                 "base_url" =>
                   "http://127.0.0.1:#{port}/Elixir.Polyjuice.Client.WellKnownTest.Httpd/notjson"
               }
             }) == {:error, :fail_error}

      :inets.stop(:httpd, httpd_pid)
    after
      File.rm_rf(tmpdir)
    end
  end
end
