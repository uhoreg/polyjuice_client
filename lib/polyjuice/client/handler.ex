# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defprotocol Polyjuice.Client.Handler do
  @moduledoc """
  Protocol for client event handlers.
  """

  @doc """
  Handle an event from the client.

  Events (in the form `type, args`) may include:

  - `:logged_in`, `{user_id, device_id, login_info}`: the user was logged in.
  - `:logged_out`, `{soft_logout}`: the user was logged out.
  - `:sync_connected`, `nil`: the sync process has connected to the homeserver
  - `:sync_disconnected`, `nil`: the sync process has been disconnected from
    the homeserver
  - `:initial_sync_completed`, `nil`: the first sync after login has completed
  - `:limited`, `{room_id, prev_batch}` a room's timeline has been limited.
    Previous messages can be fetched using the `prev_batch`
  - `:message`, `{room_id, event}`: a message event has been received
  - `:state`, `{room_id, event}`: a state event has been received
  - `:invite`, `{room_id, inviter, invite_state}`: the user was invited to a room
  - `:left`, `{room_id}`: the user left a room

  If a PID is used as a handler, the process will be sent a message of the form
  `{:polyjuice_client, type}`, if `args` is `nil`, or `{:polyjuice_client, type,
  args}` otherwise.

  If a function is used as a handler, it must have arity 2, and will be given
  `type` and `args` as its arguments.

  """
  @spec handle(handler :: Polyjuice.Client.Handler.t(), event_type :: atom, args :: tuple | nil) ::
          any
  def handle(handler, event_type, args \\ nil)
end

defimpl Polyjuice.Client.Handler, for: PID do
  def handle(pid, event_type, nil) do
    send(pid, {:polyjuice_client, event_type})
  end

  def handle(pid, event_type, args) do
    send(pid, {:polyjuice_client, event_type, args})
  end
end

defimpl Polyjuice.Client.Handler, for: Function do
  def handle(func, event_type, args) do
    apply(func, [event_type, args])
  end
end
