# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PutRoomsSend do
  @moduledoc """
  Send a message event to a room.

  https://matrix.org/docs/spec/client_server/r0.5.0#put-matrix-client-r0-rooms-roomid-send-eventtype-txnid
  """

  @type t :: %__MODULE__{
          room: String.t(),
          txn_id: String.t(),
          event_type: String.t(),
          message: map
        }

  @enforce_keys [:room, :txn_id, :message]
  defstruct [
    :room,
    :txn_id,
    :message,
    event_type: "m.room.message"
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PutRoomsSend{
          room: room,
          event_type: event_type,
          message: message,
          txn_id: txn_id
        }) do
      e = &URI.encode_www_form/1
      body = Jason.encode_to_iodata!(message)

      Polyjuice.Client.Endpoint.HttpSpec.put(
        :r0,
        "rooms/#{e.(room)}/send/#{e.(event_type)}/#{e.(txn_id)}",
        body: body
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, parsed) do
      {:ok, Map.get(parsed, "event_id")}
    end
  end
end
