# Copyright 2020 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostCreateRoom do
  @moduledoc """
    Create a room with various configuration options
    https://matrix.org/docs/spec/client_server/r0.5.0#post-matrix-client-r0-createroom
  """

  @type invite3pid :: %{
          id_server: String.t(),
          id_access_token: String.t(),
          medium: String.t()
        }

  @type state_event :: %{
          type: String.t(),
          state_key: String.t(),
          content: map()
        }

  @type t :: %__MODULE__{
          visibility: :private | :public,
          room_alias_name: String.t() | nil,
          name: String.t() | nil,
          topic: String.t() | nil,
          invite: list(String.t()),
          invite_3pid: list(invite3pid()),
          room_version: String.t() | nil,
          creation_content: map() | nil,
          initial_state: list(state_event()),
          preset: :private_chat | :public_chat | :trusted_private_chat | nil,
          is_direct: boolean(),
          power_level_content_override: map() | nil
        }

  @enforce_keys []

  defstruct [
    :visibility,
    :room_alias_name,
    :name,
    :topic,
    :creation_content,
    :preset,
    :is_direct,
    :power_level_content_override,
    invite: [],
    invite_3pid: [],
    room_version: nil,
    initial_state: []
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PostCreateRoom{} = create_room_struct) do
      body =
        create_room_struct
        |> Map.from_struct()
        |> Enum.reject(fn {_, v} -> is_nil(v) || v == [] end)
        |> Map.new()
        |> Jason.encode_to_iodata!()

      Polyjuice.Client.Endpoint.HttpSpec.post(
        :r0,
        "createRoom",
        body: body,
        auth_required: true
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end

    defimpl Polyjuice.Client.Endpoint.BodyParser do
      def parse(_req, parsed) do
        {:ok, Map.get(parsed, "room_id")}
      end
    end
  end
end
