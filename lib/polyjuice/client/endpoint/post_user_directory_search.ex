# Copyright 2021 Ketsapiwiq <ketsapiwiq@protonmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostUserDirectorySearch do
  @moduledoc """
  Performs a search for users.

  https://matrix.org/docs/spec/client_server/r0.6.1#post-matrix-client-r0-user-directory-search
  """

  @type t :: %__MODULE__{
          search_term: String.t(),
          limit: integer | nil
        }

  @enforce_keys [:search_term]
  defstruct [
    :search_term,
    :limit
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PostUserDirectorySearch{
          search_term: search_term,
          limit: limit
        }) do
      body =
        [
          [{"search_term", search_term}],
          if(not is_nil(limit), do: [{"limit", limit}], else: [])
        ]
        |> Enum.concat()
        |> Map.new()
        |> Jason.encode_to_iodata!()

      Polyjuice.Client.Endpoint.HttpSpec.post(
        :r0,
        "user_directory/search",
        body: body
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end
end
