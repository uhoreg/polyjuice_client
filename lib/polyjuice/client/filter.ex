# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Filter do
  @moduledoc ~S"""
  Build filters.

  https://matrix.org/docs/spec/client_server/r0.5.0#filtering

  The functions in this module can be chained to create more complex filters.

  Examples:

      iex> Polyjuice.Client.Filter.include_state_types(["m.room.member"])
      ...> |> Polyjuice.Client.Filter.limit_timeline_events(10)
      ...> |> Polyjuice.Client.Filter.lazy_loading()
      %{
        "room" => %{
          "state" => %{
            "types" => ["m.room.member"],
            "lazy_load_members" => true
          },
          "timeline" => %{
            "lazy_load_members" => true,
            "limit" => 10
          }
        }
      }

  """

  @doc """
  Update the value in a filter.

  The `key_path` is a list of keys to traverse to find the element to update.
  The `initial` and `func` arguments are like the corresponding arguments to
  `Map.update`.

  Examples:

      iex> Polyjuice.Client.Filter.update(
      ...>   %{
      ...>     "presence" => %{
      ...>       "not_senders" => ["@alice:example.com"]
      ...>     }
      ...>   },
      ...>   ["presence", "not_senders"],
      ...>   ["@bob:example.com"],
      ...>   &Enum.concat(&1, ["@bob:example.com"])
      ...>  )
      %{
        "presence" => %{
          "not_senders" => ["@alice:example.com", "@bob:example.com"]
        }
      }

      iex> Polyjuice.Client.Filter.update(
      ...>   %{
      ...>     "presence" => %{
      ...>       "not_senders" => ["@alice:example.com"]
      ...>     }
      ...>   },
      ...>   ["presence", "senders"],
      ...>   ["@bob:example.com"],
      ...>   &Enum.concat(&1, ["@bob:example.com"])
      ...>  )
      %{
        "presence" => %{
          "not_senders" => ["@alice:example.com"],
          "senders" => ["@bob:example.com"]
        }
      }

  """
  @spec update(map, [String.t()], any, (any -> any)) :: map
  def update(filter, key_path, initial, func)

  def update(filter, [key], initial, func) when is_map(filter) do
    Map.update(filter, key, initial, func)
  end

  def update(filter, [key | rest], initial, func) when is_map(filter) do
    Map.put(filter, key, update(Map.get(filter, key, %{}), rest, initial, func))
  end

  @doc """
  Set the value in a filter.

  The `key_path` is a list of keys to traverse to find the element to update.
  The `initial` and `func` arguments are like the corresponding arguments to
  `Map.update`.

      iex> Polyjuice.Client.Filter.put(
      ...>   %{
      ...>     "presence" => %{
      ...>       "not_senders" => ["@alice:example.com"]
      ...>     }
      ...>   },
      ...>   ["presence", "not_senders"],
      ...>   ["@bob:example.com"]
      ...>  )
      %{
        "presence" => %{
          "not_senders" => ["@bob:example.com"]
        }
      }

  """
  @spec put(map, [String.t()], any) :: map
  def put(filter, key_path, val)

  def put(filter, [key], val) when is_map(filter) do
    Map.put(filter, key, val)
  end

  def put(filter, [key | rest], val) when is_map(filter) do
    Map.put(filter, key, put(Map.get(filter, key, %{}), rest, val))
  end

  defmodule Event do
    @moduledoc """
    Create `EventFilter`s, `RoomEventFilter`s, `StateFilter`s, or `RoomFilter`s.

    https://matrix.org/docs/spec/client_server/latest#post-matrix-client-r0-user-userid-filter

    These are filters that form part of a `Polyjuice.Client.Filter`.  Not all of
    the functions in here may apply to all the different types of filters.
    """

    @doc """
    Add items to a list parameter in the filter.
    """
    @spec add_to_list(filter :: map, name :: String.t(), data :: list) :: map
    def add_to_list(filter \\ %{}, name, data)
        when is_map(filter) and is_binary(name) and is_list(data) do
      Polyjuice.Client.Filter.update(
        filter,
        [name],
        data,
        &Enum.concat(&1, data)
      )
    end

    @doc """
    Include certain types of events.
    """
    @spec include_types(filter :: map, types :: list) :: map
    def include_types(filter \\ %{}, types) when is_map(filter) and is_list(types),
      do: add_to_list(filter, "types", types)

    @doc """
    Exclude certain types of events.
    """
    @spec exclude_types(filter :: map, types :: list) :: map
    def exclude_types(filter \\ %{}, types) when is_map(filter) and is_list(types),
      do: add_to_list(filter, "not_types", types)

    @doc """
    Include certain senders.
    """
    @spec include_senders(filter :: map, types :: list) :: map
    def include_senders(filter \\ %{}, senders) when is_map(filter) and is_list(senders),
      do: add_to_list(filter, "senders", senders)

    @doc """
    Exclude certain senders.
    """
    @spec exclude_senders(filter :: map, senders :: list) :: map
    def exclude_senders(filter \\ %{}, senders) when is_map(filter) and is_list(senders),
      do: add_to_list(filter, "not_senders", senders)

    @doc """
    Include certain rooms.
    """
    @spec include_rooms(filter :: map, rooms :: list) :: map
    def include_rooms(filter \\ %{}, rooms) when is_map(filter) and is_list(rooms),
      do: add_to_list(filter, "rooms", rooms)

    @doc """
    Exclude certain rooms.
    """
    @spec exclude_rooms(filter :: map, rooms :: list) :: map
    def exclude_rooms(filter \\ %{}, rooms) when is_map(filter) and is_list(rooms),
      do: add_to_list(filter, "not_rooms", rooms)

    @doc """
    Set the maximum number of events to return.
    """
    @spec limit(filter :: map, limit :: integer) :: map
    def limit(filter \\ %{}, limit) when is_map(filter) and is_integer(limit),
      do: Map.put(filter, "limit", limit)

    @doc """
    Set whether to include or exclude events that have a URL.

    If `flag` is `true`, only events that have a `url` field will be included.
    If `flag` is `false`, events that have a `url` field will be excluded.
    """
    @spec contains_url(filter :: map, flag :: boolean) :: map
    def contains_url(%{} = filter \\ %{}, flag) when is_map(filter) and is_boolean(flag),
      do: Map.put(filter, "contains_url", flag)
  end

  @doc """
  Update the presence filter using a function.
  """
  @spec presence(filter :: map, f :: function) :: map
  def presence(filter, f) when is_function(f) do
    Map.put(filter, "presence", f.(Map.get(filter, "presence", %{})))
  end

  @doc """
  Allow certain types of presence events to be included.
  """
  @spec include_presence_types(filter :: map, types :: list) :: map
  def include_presence_types(filter \\ %{}, types) when is_map(filter) and is_list(types) do
    presence(filter, &Polyjuice.Client.Filter.Event.include_types(&1, types))
  end

  @doc """
  Don't allow certain types of presence events.
  """
  @spec exclude_presence_types(filter :: map, types :: list) :: map
  def exclude_presence_types(filter \\ %{}, types) do
    presence(filter, &Polyjuice.Client.Filter.Event.exclude_types(&1, types))
  end

  @doc """
  Update the ephemeral filter using a function.
  """
  @spec ephemeral(filter :: map, f :: function) :: map
  def ephemeral(filter, f) when is_function(f) do
    update(
      filter,
      ["room", "ephemeral"],
      f.(%{}),
      f
    )
  end

  @doc """
  Allow certain types of ephemeral room events to be included.
  """
  @spec include_ephemeral_types(filter :: map, types :: list) :: map
  def include_ephemeral_types(filter \\ %{}, types) do
    ephemeral(filter, &Polyjuice.Client.Filter.Event.include_types(&1, types))
  end

  @doc """
  Don't allow certain types of ephemeral room events.
  """
  @spec exclude_ephemeral_types(filter :: map, types :: list) :: map
  def exclude_ephemeral_types(filter \\ %{}, types) do
    ephemeral(filter, &Polyjuice.Client.Filter.Event.exclude_types(&1, types))
  end

  @doc """
  Update the state filter using a function.
  """
  @spec state(filter :: map, f :: function) :: map
  def state(filter, f) when is_function(f) do
    update(
      filter,
      ["room", "state"],
      f.(%{}),
      f
    )
  end

  @doc """
  Allow certain types of state events to be included.
  """
  @spec include_state_types(filter :: map, types :: list) :: map
  def include_state_types(filter \\ %{}, types) do
    state(filter, &Polyjuice.Client.Filter.Event.include_types(&1, types))
  end

  @doc """
  Don't allow certain types of state events.
  """
  @spec exclude_state_types(filter :: map, types :: list) :: map
  def exclude_state_types(filter \\ %{}, types) do
    state(filter, &Polyjuice.Client.Filter.Event.exclude_types(&1, types))
  end

  @doc """
  Update the timeline filter using a function.
  """
  @spec timeline(filter :: map, f :: function) :: map
  def timeline(filter, f) when is_function(f) do
    update(
      filter,
      ["room", "timeline"],
      f.(%{}),
      f
    )
  end

  @doc """
  Allow certain types of timeline events to be included.
  """
  @spec include_timeline_types(filter :: map, types :: list) :: map
  def include_timeline_types(filter \\ %{}, types) do
    timeline(filter, &Polyjuice.Client.Filter.Event.include_types(&1, types))
  end

  @doc """
  Don't allow certain types of timeline events.
  """
  @spec exclude_timeline_types(filter :: map, types :: list) :: map
  def exclude_timeline_types(filter \\ %{}, types) do
    timeline(filter, &Polyjuice.Client.Filter.Event.exclude_types(&1, types))
  end

  @doc """
  Set the maximum number of timeline events.
  """
  @spec limit_timeline_events(filter :: map, limit :: integer) :: map
  def limit_timeline_events(filter \\ %{}, limit) do
    timeline(filter, &Polyjuice.Client.Filter.Event.limit(&1, limit))
  end

  @spec lazy_loading(filter :: map) :: map
  def lazy_loading(filter \\ %{})

  def lazy_loading(filter) when filter == %{} do
    %{
      "room" => %{
        "state" => %{
          "lazy_load_members" => true
        },
        "timeline" => %{
          "lazy_load_members" => true
        }
      }
    }
  end

  def lazy_loading(filter) when is_map(filter) do
    filter
    |> put(["room", "state", "lazy_load_members"], true)
    |> put(["room", "timeline", "lazy_load_members"], true)
  end
end
